# Task Nr. 1 #

1. Create an html page based on the psd file.

## Requirements ##

1. The page should be responsive.
2. Use the correct fonts from the google fonts library ("Dosis" - headlines, "Source Sans Pro" - copy and small titles).
3. From 720px width, hide the navigation and create a mobile navigation with a "hamburger" style action button in the right side of the header. You need to decide the type and the style of the mobile navigation.
4. Do not minify (or concatenate the stylesheets)!
5. Browser requirements: IE9+, Chrome/FF/Safari

## Hints/Tipps ##

1. You can use any frontend framework what you would like.
2. You can (should) use SASS or any other CSS preprocessor.
3. The text box in the header image doesn't need to be a separate html block, it can be inside the image.