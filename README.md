# About the tests #

We prepared two tests. With the first one you can show us your frontend/layout skills, the second is a little bit more complex, probably needs some research. The overview you can find **[HERE][2]**, if you enter the directories, you will find the requirements and some infos.

You can download the repository as a .zip **[HERE][1]**.

The sourcecodes you can put on github or you can compress and send it in email.

If you have any questions, please don't hesitate to contact me ([zoltan.seer@bss-neon.de][3]).

Good luck,   
Zoltan Seer

[1]: https://bitbucket.org/bssneon/interview-test/downloads
[2]: https://bitbucket.org/bssneon/interview-test/src
[3]: mailto:zoltan.seer@bss-neon.de